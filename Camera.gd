extends Camera

var move_speed = 0.1
var rotation_speed = PI/2

func _process(delta: float) -> void:
	if Input.is_key_pressed(KEY_W):
		transform.origin.z -= move_speed
	if Input.is_key_pressed(KEY_S):
		transform.origin.z += move_speed
	if Input.is_key_pressed(KEY_A):
		transform.origin.x -= move_speed
	if Input.is_key_pressed(KEY_D):
		transform.origin.x += move_speed
	var y_rotation = 0
	if Input.is_key_pressed(KEY_Q):
		y_rotation += 1
		rotate_object_local(Vector3.UP, y_rotation * rotation_speed * delta)
	if Input.is_key_pressed(KEY_E):
		y_rotation -= 1
		rotate_object_local(Vector3.UP, y_rotation * rotation_speed * delta)
